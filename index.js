'use strict'

const Koa = require('koa');
const Router = require('koa-router');
const bodyparser = require('koa-bodyparser');
const cors = require('@koa/cors');

const app = new Koa();
const router = new Router();

// middlewares

app.use(cors());
app.use(bodyparser());
app.use(require('./middlewares/error'));

// routes
const mainRouter = require('./routes');
router.use('/transactions', mainRouter.routes(), mainRouter.allowedMethods());

app.use(router.routes(), router.allowedMethods());

module.exports = app.listen(process.env.PORT || 3003, () => {
  console.log('Server listening on port ', process.env.PORT || 3003)
});
