'use strict';

module.exports = async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    console.log(err);
    ctx.body = {
      error: {
        message: err.message,
        code: err.code
      }
    };
    ctx.status = err.status || 500;
  }
};
