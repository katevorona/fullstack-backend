# fullstack-backend

# RUNNIG

To run this application write in terminal 
`npm install` 
and then 
`npm run start`

To run tests write in terminal `npm run test`



# API:

POST http://localhost:3003/transactions

Body: 
`{
	"id": "SOMETRANSACTIONS-142",
	"type": "credit",
	"amount": 16,
	"effectiveDate": "2018-04-06T14:36:30.243Z" 
}`


GET http://localhost:3003/transactions 

GET http://localhost:3003/transactions/:id

DELETE http://localhost:3003/transactions 
