'use strict';

const requestValidation = require('../middlewares/requestValidator');

function createValidator(schema, errorPrefix, forBody) {
  return async function (ctx, next) {
    const validator = requestValidation.validator({
      schema,
      ajvSettings: {
        removeAdditional: true,
        coerceTypes: true
      },
      errorPrefix
    });
    try {
      validator(forBody ? ctx.request.body : ctx.params)
    } catch (err) {
      ctx.throw(400, err.message, { code: err.code })
    }
    return await next()
  }
}

module.exports = {
  createBodyValidator: (schema, prefix) => createValidator(schema, prefix, true),
  createParamsValidator: (schema, prefix) => createValidator(schema, prefix)
};
