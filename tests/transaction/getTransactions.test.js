const request = require('superagent');
const mockTransaction = require('./mockTransaction');
const expect = require('chai').expect;

describe('get transactions list', function () {
  it('should return empty result if no one transaction exists', async () => {
    const transactionResponse = await request.get('http://localhost:3003/transactions').catch(r => r);
    expect(transactionResponse.status).equal(200);
    expect(transactionResponse.body).deep.equal([]);
  });

  it('should return all existed transaction ', async () => {
    const firstTransaction = mockTransaction();
    const secondTransaction = mockTransaction({id: 'SECONDTRANSACTIONID'});

    await request
      .post('http://localhost:3003/transactions')
      .send(firstTransaction);

    await request
      .post('http://localhost:3003/transactions')
      .send(secondTransaction);

    const transactionResponse = await request.get('http://localhost:3003/transactions').catch(r => r);
    expect(transactionResponse.status).equal(200);
    expect(transactionResponse.body.length).equal(2);
    expect(transactionResponse.body[0]).deep.equal(firstTransaction);
    expect(transactionResponse.body[1]).deep.equal(secondTransaction);
  })
});
