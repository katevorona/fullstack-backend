const cache = require('memory-cache');
let server;
before(function () {
  this.timeout(5000)
  server = require('../index')
});

beforeEach(async () => {
  cache.clear()
});

after(() => {
  server.close()
});

require('./transaction');
