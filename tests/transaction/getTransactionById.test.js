const request = require('superagent');
const mockTransaction = require('./mockTransaction');
const expect = require('chai').expect;

describe('get transaction by id', function () {
  it('should return 404 error if no transaction with such id exist', async () => {
    const transactionResponse = await request.get('http://localhost:3003/transactions/1').catch(r => r);
    expect(transactionResponse.status).equal(404);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'TRANSACTION_NOT_FOUND',
        message: 'Transaction with this id is not found'
      }
    });
  });

  it('should return 400 error if id is not correct type', async () => {
    const transactionResponse = await request.get('http://localhost:3003/transactions/!!!!').catch(r => r);
    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'GET_TRANSACTION_ID_WRONG',
        message: 'wrong property: \'id\''
      }
    });
  });

  it('should return transaction by id', async () => {
    const transaction = mockTransaction();

    await request
      .post('http://localhost:3003/transactions')
      .send(transaction)

    const transactionResponse = await request.get(`http://localhost:3003/transactions/${transaction.id}`).catch(r => r);
    expect(transactionResponse.status).equal(200);
    expect(transactionResponse.body).deep.equal(transaction);
  })
});
