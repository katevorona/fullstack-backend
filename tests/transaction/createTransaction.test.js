const request = require('superagent');
const mockTransaction = require('./mockTransaction');
const expect = require('chai').expect

describe('create transaction', function () {
  it('should return validation error if id field does not exist', async () => {
    const transaction = mockTransaction();
    delete transaction.id;
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(transaction)
      .catch(r => r);

    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'CREATE_TRANSACTION_ID_MISSING',
        message: 'id missing'
      }
    })
  });

  it('should return validation error if id is not correct', async () => {
    const transaction = mockTransaction({id: '!!!!!'});
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(transaction)
      .catch(r => r);

    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'CREATE_TRANSACTION_ID_WRONG',
        message: 'wrong property: \'id\''
      }
    })
  });

  it('should return validation error if type field does not exist', async () => {
    const transaction = mockTransaction();
    delete transaction.type;
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(transaction)
      .catch(r => r);

    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'CREATE_TRANSACTION_TYPE_MISSING',
        message: 'type missing'
      }
    })
  });

  it('should return validation error if type is not correct', async () => {
    const transaction = mockTransaction({type: 'hello'});
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(transaction)
      .catch(r => r);

    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'CREATE_TRANSACTION_TYPE_WRONG',
        message: 'wrong property: \'type\''
      }
    })
  });

  it('should return validation error if amount field does not exist', async () => {
    const transaction = mockTransaction();
    delete transaction.amount;
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(transaction)
      .catch(r => r);

    expect(transactionResponse.status).equal(400)
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'CREATE_TRANSACTION_AMOUNT_MISSING',
        message: 'amount missing'
      }
    })
  });

  it('should return validation error if amount is not correct', async () => {
    const transaction = mockTransaction({amount: 'hello'});
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(transaction)
      .catch(r => r);

    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'CREATE_TRANSACTION_AMOUNT_WRONG',
        message: 'wrong property: \'amount\''
      }
    })
  });

  it('should return validation error if amount less then 0', async () => {
    const transaction = mockTransaction({amount: -5});
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(transaction)
      .catch(r => r);

    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'CREATE_TRANSACTION_AMOUNT_WRONG',
        message: 'wrong property: \'amount\''
      }
    })
  });

  it('should return validation error if effectiveDate field does not exist', async () => {
    const transaction = mockTransaction();
    delete transaction.effectiveDate;
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(transaction)
      .catch(r => r);

    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'CREATE_TRANSACTION_EFFECTIVEDATE_MISSING',
        message: 'effectiveDate missing'
      }
    })
  });

  it('should return validation error if effectiveDate is not correct', async () => {
    const transaction = mockTransaction({effectiveDate: 'fdjfdjfd'});
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(transaction)
      .catch(r => r);

    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'CREATE_TRANSACTION_EFFECTIVEDATE_WRONG',
        message: 'wrong property: \'effectiveDate\''
      }
    })
  });

  it('should create transaction successfully', async () => {
    const transactionResponse = await request
      .post('http://localhost:3003/transactions')
      .send(mockTransaction())
      .catch(r => r);

    expect(transactionResponse.status).equal(201)
  })
});
