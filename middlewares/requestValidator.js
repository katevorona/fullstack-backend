'use strict';
const Ajv = require('ajv');

function ValidationError(message, code) {
  this.code = (code || "VALIDATION_ERROR");
  this.message = (message || "");
}
ValidationError.prototype = Error.prototype;

function validatorFactory(validationObject) {

  const ajv = new Ajv(Object.assign(
    { allErrors: true },
    validationObject.ajvSettings
  ));

  ajv.addSchema(validationObject.schema);

  return function (body) {
    const errorPrefix = validationObject.errorPrefix;
    const isValid = ajv.validate(validationObject.schema, body);

    if (!isValid) {

      const error = ajv.errors[0];

      if (error.keyword.includes('required')) {
        const missingProp = error.params.missingProperty;
        throw new ValidationError(`${missingProp} missing`, `${errorPrefix}_${missingProp.toUpperCase()}_MISSING`);
      }

      if (error.dataPath && error.dataPath.startsWith('.')) {
        const wrongProp = error.dataPath.substr(1);
        throw new ValidationError(`wrong property: '${wrongProp}'`, `${errorPrefix}_${wrongProp.toUpperCase()}_WRONG`);
      }

      throw new ValidationError('Unkown Error', `${errorPrefix}_UNKOWN_ERROR`);
    }
  }
}

module.exports = {
  validator: validatorFactory
};

