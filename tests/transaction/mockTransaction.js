'use strict';

function mockTransaction(overrideValues) {
  const defaultValues = {
    "id": "SOMEID",
    "type": "credit",
    "amount": 5,
    "effectiveDate": "2018-04-06T14:36:30.243Z"
  };
  return Object.assign({}, defaultValues, overrideValues);
}

module.exports = mockTransaction;
