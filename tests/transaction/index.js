describe('Company', () => {
  require('./getTransactionById.test');
  require('./getTransactions.test');
  require('./createTransaction.test');
  require('./deleteTransaction.test');
})
