'use strict';

const cache = require('memory-cache');
const validator = require('../../lib/validator');

const schema = {
  properties: {
    id: {type: 'string', pattern: '[a-z,A-Z,0-9,-]+'}
  },
  required: ['id']
};

async function deleteTransaction(ctx) {
  const transactionId = ctx.params.id;
  if(!cache.get(transactionId)) {
    ctx.throw(404, {code: "TRANSACTION_NOT_FOUND", message: "Transaction with this id is not found"});
  }
  cache.del(transactionId);
  ctx.status = 204;
}

module.exports = {
  handler: deleteTransaction,
  paramsValidator: validator.createParamsValidator(schema, 'DELETE_TRANSACTION')
};

