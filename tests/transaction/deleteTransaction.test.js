const request = require('superagent');
const mockTransaction = require('./mockTransaction');
const expect = require('chai').expect;

describe('delete transaction', function () {
  it('should return 404 error if no transaction with such id exist', async () => {
    const transactionResponse = await request.delete('http://localhost:3003/transactions/1').catch(r => r);
    expect(transactionResponse.status).equal(404);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'TRANSACTION_NOT_FOUND',
        message: 'Transaction with this id is not found'
      }
    })
  });

  it('should return 400 error if id is not integer', async () => {
    const transactionResponse = await request.delete('http://localhost:3003/transactions/!!!').catch(r => r);
    expect(transactionResponse.status).equal(400);
    expect(transactionResponse.response.body).deep.equal({
      error: {
        code: 'DELETE_TRANSACTION_ID_WRONG',
        message: 'wrong property: \'id\''
      }
    })
  });

  it('should delete transaction successfully', async () => {
    await request
      .post('http://localhost:3003/transactions')
      .send(mockTransaction())
      .catch(r => r);

   const transactionResponse = await request
      .delete(`http://localhost:3003/transactions/${mockTransaction().id}`)
      .catch(r => r);

    expect(transactionResponse.status).equal(204)
  })
});
