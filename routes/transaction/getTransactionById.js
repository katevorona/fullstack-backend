'use strict';

const cache = require('memory-cache');
const validator = require('../../lib/validator');

const schema = {
  properties: {
    id: {type: 'string', pattern: '[a-z,A-Z,0-9,-]+'}
  },
  required: ['id']
};

async function getTransactionById(ctx) {
  const transaction = cache.get(ctx.params.id);
  if(!transaction) {
    ctx.throw(404, {code: "TRANSACTION_NOT_FOUND", message: "Transaction with this id is not found"});
  }
  ctx.body = transaction;
  ctx.status = 200
}

module.exports = {
  handler: getTransactionById,
  paramsValidator: validator.createParamsValidator(schema, 'GET_TRANSACTION')
};
