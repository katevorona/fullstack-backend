'use strict';

const Router = require('koa-router');
const getTransactions = require('./transaction/getTransactions');
const getTransactionById = require('./transaction/getTransactionById');
const createTransaction = require('./transaction/createTransaction');
const deleteTransaction = require('./transaction/deleteTransaction');

const router = new Router()

router.get('/', getTransactions.handler);
router.get('/:id', getTransactionById.paramsValidator, getTransactionById.handler);
router.post('/', createTransaction.bodyValidator, createTransaction.handler);
router.delete('/:id', deleteTransaction.paramsValidator, deleteTransaction.handler);

module.exports = router;
