'use strict';

const cache = require('memory-cache');

async function getTransactions(ctx) {
  ctx.body = cache.keys().map((key) => cache.get(key));
  ctx.status = 200
}

module.exports = {
  handler: getTransactions,
};
