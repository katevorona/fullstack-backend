'use strict';

const cache = require('memory-cache');
const validator = require('../../lib/validator');

const schema = {
  properties: {
    id: {type: 'string', pattern: '[a-z,A-Z,0-9,-]+'},
    type: {type: 'string', enum: ['credit', 'debit']},
    amount: {type: 'integer', minimum: 0},
    effectiveDate: {type: 'string', format: 'date'}
  },
  required: ['id', 'type', 'amount', 'effectiveDate']
};

async function createTransaction(ctx) {
  const transaction = ctx.request.body;
  if(cache.get(transaction.id)) {
    ctx.throw(405, {code: "TRANSACTION_ALREADY_EXIST", message: "Transaction with this id already exist"});
  }
  cache.put(transaction.id, transaction);
  ctx.status = 201
}

module.exports = {
  handler: createTransaction,
  bodyValidator: validator.createBodyValidator(schema, 'CREATE_TRANSACTION')
};
